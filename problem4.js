//    Write a function that accesses and logs the name and city of the individual at the index position 3 in the dataset.
function acessAtIndex_3(arrayOfObject,index){
  
    console.log(`${arrayOfObject[index].name} ${arrayOfObject[index].city}`);
   
}



module.exports=acessAtIndex_3;