//    Implement a loop to access and log the city and country of each individual in the dataset.
function getCityandCountry(arrayOfObjects){
    for(let index=0;index<arrayOfObjects.length;index++){
        console.log(arrayOfObjects[index].city+" "+arrayOfObjects[index].country);
    }
}

module.exports=getCityandCountry;