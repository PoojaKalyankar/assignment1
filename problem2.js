//    Implement a function that retrieves and prints the hobbies of individuals with a specific age, say 30 years old.
function print_hobbiesbyage30(arrayOfObjects,age){
    let check=false;
    for(let index =0;index<arrayOfObjects.length;index++){
       if(arrayOfObjects[index].age==age){
          console.log(`${arrayOfObjects[index].name} hobbies at age ${arrayOfObjects[index].age} : ${arrayOfObjects[index].hobbies}`);
          check=true;
       }
    }
    if(!check){
      console.log("No one at age 33");
    }
 }

 module.exports=print_hobbiesbyage30; 